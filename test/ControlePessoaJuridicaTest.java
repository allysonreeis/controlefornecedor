/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import controller.ControlePessoaJuridica;
import model.PessoaJuridica;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author reis
 */
public class ControlePessoaJuridicaTest {
    
    PessoaJuridica umaPessoa;
    ControlePessoaJuridica umControle;
    PessoaJuridica pessoaPesquisada;
    @Before
    public void setUp() throws Exception{
        umaPessoa = new PessoaJuridica("João", "333-333", "4444-4444", "000.000.000");
        umControle = new ControlePessoaJuridica();
        umControle.adicionarPessoaJuridica(umaPessoa);
    }
    
    @Test
    public void pesquisarPessoaJuridicaTest () {
        pessoaPesquisada = umControle.pesquisarPessoaJuridica(umaPessoa.getNome());
        assertNotNull(pessoaPesquisada);
    }
    
    @Test
    public void removerPessoaJuridicaTest () {
        umControle.removerPessoaJuridica(umaPessoa);
        pessoaPesquisada = umControle.pesquisarPessoaJuridica(umaPessoa.getNome());
        assertNull(pessoaPesquisada);
    }
   
}
