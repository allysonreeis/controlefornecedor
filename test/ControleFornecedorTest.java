/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import controller.ControleFornecedor;
import model.Fornecedor;
import model.PessoaFisica;
import model.PessoaJuridica;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author reis
 */
public class ControleFornecedorTest {
    
    ControleFornecedor umControleFornecedor;
    PessoaFisica umaPessoaFisica;
    PessoaJuridica umaPessoaJuridica;
    Fornecedor pessoaPesquisada;
    @Before
    public void setUp() throws Exception {
        umaPessoaJuridica = new PessoaJuridica("Joao", "333-333", "4444-4444", "222.222.222");
        umaPessoaFisica = new PessoaFisica("José", "333-333", "4444-4444", "000.000.000-00");
        umControleFornecedor = new ControleFornecedor();
    }
    
    @Test
    public void pesquisarFornecedorTest () {
        
        umControleFornecedor.adicionar(umaPessoaFisica);
        umControleFornecedor.adicionar(umaPessoaJuridica);
        pessoaPesquisada = umControleFornecedor.pesquisarFornecedor(umaPessoaFisica.getNome());
        assertEquals(umaPessoaFisica, pessoaPesquisada);
        pessoaPesquisada = umControleFornecedor.pesquisarFornecedor(umaPessoaJuridica.getNome());
        assertEquals(umaPessoaJuridica, pessoaPesquisada);
    }
    
    @Test
    public void removerFornecedorTest () {
        umControleFornecedor.remover(umaPessoaFisica);
        pessoaPesquisada = umControleFornecedor.pesquisarFornecedor(umaPessoaFisica.getNome());
        assertNull(pessoaPesquisada);
        umControleFornecedor.remover(umaPessoaJuridica);
        pessoaPesquisada = umControleFornecedor.pesquisarFornecedor(umaPessoaJuridica.getNome());
        assertNull(pessoaPesquisada);
    }
}
