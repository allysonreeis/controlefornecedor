/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import controller.ControleProduto;
import model.Produto;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author reis
 */
public class ControleProdutoTest {
    
    Produto umProduto;
    ControleProduto umControleProduto;
    Produto produtoPesquisado;
    @Before
    public void setUp() throws Exception{
        umProduto = new Produto();
        umControleProduto = new ControleProduto();
        
        umProduto.setDescricao("Limpeza");
        umProduto.setNome("Sabão");
        umProduto.setQuantidadeEstoque(2.0);
        umProduto.setValorCompra(4.99);
        umProduto.setValorVenda(6.99);
        
        umControleProduto.adicionarProduto(umProduto);
    }
    
    @Test
    public void testPesquisarProduto() {
        produtoPesquisado = umControleProduto.pesquisaProduto(umProduto.getNome());
	assertNotNull(produtoPesquisado);
    }
	
    @Test
    public void testRemoverProduto () {
	umControleProduto.removerProduto(umProduto);
	produtoPesquisado = umControleProduto.pesquisaProduto(umProduto.getNome());
	assertNull(produtoPesquisado);
    }

}
