
package model;

import java.util.ArrayList;

public class Fornecedor {
    private String nome;
    private String rg;
    private String telefoneFixo;
    private String telefoneCelular;
    private Endereco endereco;
    private ArrayList<Produto> produtos;
    private String tipo;
    
    public Fornecedor (String nome, String telefoneFixo, String telefoneCelular) {
	this.nome = nome;
        this.telefoneFixo = telefoneFixo;
        this.telefoneCelular = telefoneCelular;
    }
    
    public String getTipo () {
        return tipo;
    }
    
    public void setTipo (String tipo) {
        this.tipo = tipo;
    }
    
    public String getTelefoneFixo() {
        return telefoneFixo;
    }

    public void setTelefoneFixo(String telefoneFixo) {
        this.telefoneFixo = telefoneFixo;
    }

    public String getTelefoneCelular() {
        return telefoneCelular;
    }

    public void setTelefoneCelular(String telefoneCelular) {
        this.telefoneCelular = telefoneCelular;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public ArrayList<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(ArrayList<Produto> produtos) {
        this.produtos = produtos;
    }
    
    public String getRg () {
        return rg;
    }
    
    public void setRg (String rg) {
        this.rg = rg;
    }
}
