
package controller;

import java.util.ArrayList;
import model.Produto;

public class ControleProduto {
    private ArrayList<Produto> listaProdutos;
    
    public ControleProduto () {
        listaProdutos = new ArrayList<Produto>();
    }

    public ArrayList<Produto> getListaProdutos() {
        return listaProdutos;
    }

    public void setListaProdutos(ArrayList<Produto> listaProdutos) {
        this.listaProdutos = listaProdutos;
    }
    
    
    public String adicionarProduto (Produto umProduto) {
        String mensagem = "Produto adicionado com sucesso!";
        listaProdutos.add(umProduto);
        return mensagem;
    }
    
    public String removerProduto (Produto umProduto) {
        String mensagem = "Produto removido com sucesso!";
        listaProdutos.remove(umProduto);
        return mensagem;
    }
    
    public Produto pesquisaProduto (String nomeProduto) {
        for (Produto umProduto : listaProdutos) {
            if (umProduto.getNome().equalsIgnoreCase(nomeProduto)) return umProduto;
        }
        return null;
    }
}
