
package controller;

import java.util.ArrayList;
import model.PessoaJuridica;

public class ControlePessoaJuridica {
    
    private ArrayList<PessoaJuridica> listaPessoaJuridica;
    
    public ControlePessoaJuridica () {
        listaPessoaJuridica = new ArrayList<PessoaJuridica>();
    }

    public ArrayList<PessoaJuridica> getListaPessoaJuridica() {
        return listaPessoaJuridica;
    }

    public void setListaPessoaJuridica(ArrayList<PessoaJuridica> listaPessoaJuridica) {
        this.listaPessoaJuridica = listaPessoaJuridica;
    }
    
    
    
    public String adicionarPessoaJuridica (PessoaJuridica umaPessoa) {
        String mensagem = "Pessoa Jurídica, adicionada com sucesso!";
        listaPessoaJuridica.add(umaPessoa);
        return mensagem;
    }
    
    public String removerPessoaJuridica (PessoaJuridica umaPessoa) {
        String mensagem = "Pessoa Jurídica, removida com sucesso!";
        listaPessoaJuridica.remove(umaPessoa);
        return mensagem;
    }
    
    public PessoaJuridica pesquisarPessoaJuridica (String umNome) {
        for (PessoaJuridica umaPessoa : listaPessoaJuridica) {
            if (umaPessoa.getNome().equalsIgnoreCase(umNome)) return umaPessoa;
        }
        return null;
    }
}
